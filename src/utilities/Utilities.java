package utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

import org.json.JSONObject;

public class Utilities {

	/**
	 * Tries to read the given filename into a string
	 * 
	 * @param filename	Target file
	 * @return			A string of the file's contents, or null if there is an error
	 */
	public static String readFile(String filename) {
		File inputFile = new File(filename);
		FileInputStream in;

		if (!inputFile.exists()) {
			return null;
		}

		try {
			in = new FileInputStream(inputFile);

			byte[] buffer = new byte[in.available()];
			in.read(buffer);
			String input = new String(buffer);

			in.close();
			return input;


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Writes a vector to the given filename, each element on a newline
	 * 
	 * @param strings		Strings to write
	 * @param filename		Target file
	 */
	public static void writeFile(Vector<String> strings, String filename) {
		File outFile = new File(filename);
		try {
			FileWriter out = new FileWriter(outFile);

			for (int i = 0; i < strings.size(); i++) {
				out.write(strings.get(i) + "\n");
			}

			out.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** 
	 * Makes a Twitch api call to see if the target stream is online
	 * 
	 * @param stream	Target stream
	 * @return			Whether or not the stream is online
	 */
	public static boolean isStreamOnline(String stream) {

		String targetURL = "https://api.twitch.tv/kraken/streams/" + stream;

		URL url;
		HttpURLConnection connection = null;

		try {
			url = new URL(targetURL);
			connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/vnd.twitchtv.v2+json");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");


			connection.setUseCaches(false);
			connection.setDoOutput(true);
			connection.setDoInput(true);

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line ;
			StringBuffer response = new StringBuffer();

			while ((line = in.readLine()) != null) {
				response.append(line);
			}

			connection.disconnect();
			in.close();

			JSONObject jsonResponse = new JSONObject(response.toString());

			return !jsonResponse.isNull("stream");
		} catch (IOException e) {
			return false;
		}

	}

	/**
	 * Executes a sql query and returns the result set
	 * 
	 * @param formatStatement	Format for the prepared statement
	 * @param args				Arguments for the prepared statement
	 * @return					Result set
	 */
	public static ResultSet executeSQLQuery(String formatStatement, ArrayList<String> args) {
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet result = null;

		// Need credentials
		String url = "";
		String user = "";
		String pass = "";

		try {
			con = DriverManager.getConnection(url, user, pass);
			statement = con.prepareStatement(formatStatement);

			if (args != null) {

				for (int i = 0; i < args.size(); i++) {
					statement.setString(i + 1, args.get(i));
				}

			}

			result = statement.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Executes a sql statement which doesn't have a return value
	 * 
	 * @param formatStatement		Format string for the prepared statement
	 * @param args					Args for the prepared statement
	 */
	public static void executeSQLUpdate(String formatStatement, ArrayList<String> args) {
		Connection con = null;
		PreparedStatement statement = null;

		// Need credentials
		String url = "";
		String user = "";
		String pass = "";

		try {
			con = DriverManager.getConnection(url, user, pass);
			statement = con.prepareStatement(formatStatement);

			if (args != null) {

				for (int i = 0; i < args.size(); i++) {
					statement.setString(i + 1, args.get(i));
				}

			}

			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Executes a batch of sql statements with no return
	 * 
	 * @param formatStatement		Format string for the prepared statement
	 * @param batchArgs				A list of args for every individual prepared statement
	 */
	public static void executeSQLBatchUpdate(String formatStatement, ArrayList<ArrayList<String>> batchArgs) {
		Connection con = null;
		PreparedStatement statement = null;

		// Need credentials
		String url = "";
		String user = "";
		String pass = "";

		try {
			con = DriverManager.getConnection(url, user, pass);
			statement = con.prepareStatement(formatStatement);

			if (batchArgs != null) {

				for (int j = 0; j < batchArgs.size(); j++) {
					// Grab the args for the current batch command
					ArrayList<String> args = batchArgs.get(j);
					
					// Create the statement
					for (int i = 0; i < args.size(); i++) {
						statement.setString(i + 1, args.get(i));
					}
					
					// Add it to the batch command
					statement.addBatch();

				}

			}

			// Execute the batch
			statement.executeBatch();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
