package main;

import java.util.Vector;

import threads.Thread_MessageSender;
import threads.Thread_MessageTimer;

/**
 * Used to prevent channels from sending more than 20 messages
 * Every 30 seconds
 * 
 * @author Cadorn
 *
 */
public class MessageController {

	// There are no messages to send
	public volatile Object noMessageLock;
	// We have sent too many messages
	public volatile Object tooManyMessagesLock;
	// A list of all messages to send
	public volatile Vector<String> messages;

	// Message cap
	// Is actually 30, but we want to be safe
	private final static int MESSAGE_CAP = 20;
	// How many messages have been sent in this interval
	private int messagesSent;

	private BotController bot;

	// Timer to clear messages sent per interval
	private Thread_MessageTimer timer;
	// The thread used to perform the actual sending
	private Thread_MessageSender sender;
	
	private String channelName;

	public MessageController(BotController bot, String channelName) {
		this.bot = bot;
		this.channelName = channelName;

		noMessageLock = new Object();
		tooManyMessagesLock = new Object();

		messages= new Vector<String>();

		timer = new Thread_MessageTimer(this);
		sender = new Thread_MessageSender(this);

		sender.start();

		this.messagesSent = 0;
	}

	/**
	 * Adds a message to the queue
	 * Notifies the message sender
	 * 
	 * @param message
	 */
	public synchronized void addMessage(String message) {

		this.messages.add(message);
		synchronized(this.noMessageLock) {
			this.noMessageLock.notify();
		}
	}

	/**
	 * Returns if we can send more messages in this interval
	 * True if messages sent is less than the message cap
	 * 
	 * @return
	 */
	public synchronized boolean canSendMessage() {
		return messagesSent < MESSAGE_CAP;
	}

	/**
	 * Resets the number of messages sent this interval
	 * Alerts the message sender
	 */
	public synchronized void resetMessages() {
		this.messagesSent = 0;
		synchronized(this.tooManyMessagesLock) {
			tooManyMessagesLock.notify();
		}
	}

	/**
	 * Sends a message
	 * Starts the timer if it's a new interval
	 * 
	 */
	public synchronized void sendMessage() {
		System.out.printf("Sending Message \"%s\": %d messages sent\n", messages.get(0), messagesSent);
		if (messagesSent == 0) {
			timer = new Thread_MessageTimer(this);
			timer.start();
		}
		messagesSent++;
		bot.sendMessage(messages.get(0), this.channelName);
		messages.remove(0);
	}

}
