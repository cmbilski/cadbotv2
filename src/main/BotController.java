package main;

import input.TaggedMessage;

import java.util.HashMap;
import java.util.Vector;

import threads.Thread_ChannelCreator;
import threads.Thread_ChannelHandler;
import modules.*;

/**
 * 
 * Stores and controls all thread managers
 * Brain of the bot
 * 
 * @author Cadorn
 *
 */
public class BotController {

	// Message queues for each channel
	private HashMap<String, Vector<TaggedMessage>> messageQueues;
	// Thread for each channel
	private HashMap<String, Thread_ChannelHandler> channels;
	// IRC handler
	private IRCController ircBot;

	// Name of the bot
	private String botName;
	
	// Used to control spam at startup
	private boolean initialization;

	public static void main(String [] args) {
		new BotController();
	}

	public BotController() {
		this.messageQueues = new HashMap<String, Vector<TaggedMessage>>();
		this.channels = new HashMap<String, Thread_ChannelHandler>();

		this.initialization = true;
		
		ircBot = new IRCController(this);
		
		this.botName = ircBot.getName();

		addHostChannel();		
		
		this.initialization = false;
	}

	/**
	 * Places the message into the queue for its channel
	 * 
	 * @param input The message to be placed
	 */
	public void queueMessage(TaggedMessage input) {
		// Get the message queue for the source channel
		Vector<TaggedMessage> channelQueue = messageQueues.get(input.getTag(2));

		if (channelQueue == null) {
			// Queue for that channel does not exist
			return;
		}

		// Add the message to the queue
		channelQueue.add(input);
		// Alert the channel that it has a new method
		channels.get(input.getTag(2)).unlock();

	}

	/**
	 * Creates a host channel thread
	 * Has a default loadout of modules to allow creation of channels
	 */
	private void addHostChannel() {
		String channelName = "#" + this.botName;
		
		// We don't want duplicate channels
		if (channels.get(channelName) != null) {
			return;
		}

		// Create the message queue and channel
		Vector<TaggedMessage> newQueue = new Vector<TaggedMessage>();
		Thread_ChannelHandler newChannel = new Thread_ChannelHandler(this, newQueue, channelName);

		// Load the relevant modules into thread
		newChannel.addModule(new Module_Host(newChannel, this));
		newChannel.addModule(new Module_Greeter(newChannel));
		
		// Store the thread and queue into maps
		messageQueues.put(channelName, newQueue);
		channels.put(channelName, newChannel);

		// Start the thread
		newChannel.start();

		ircBot.joinChannel(channelName);
	}

	/**
	 * Creates a user channel thread
	 * 
	 * @param channelName
	 */
	public void addUserChannel(String channelName) {
		
		// Create a new thread to create the bot
		Thread_ChannelCreator thread = new Thread_ChannelCreator(this, channelName);
		thread.start();
		
	}

	/**
	 * Removes the specified user channel thread
	 * 
	 * @param channelName
	 */
	public void removeUserChannel(String channelName) {
		Vector<TaggedMessage> messageQueue = messageQueues.get(channelName);
		Thread_ChannelHandler channel = channels.get(channelName);

		// We can't remove channels that don't exist
		if (messageQueue == null) {
			return;
		}

		// Stop the channel thread
		channel.halt();

		messageQueues.remove(channelName);
		channels.remove(channelName);

	}

	/**
	 * Returns the name of this bot
	 * 
	 * @return The name of the bot
	 */
	public String getName() {
		return this.botName;
	}

	/**
	 * Passes a message long to the IRC controller
	 * 
	 * @param message	The message to be sent
	 * @param channelName	The channel to send the message on
	 */
	public void sendMessage(String message, String channelName) {
		ircBot.sendMessage(channelName, message);
	}

	/**
	 * Returns the message queues
	 * 
	 * @return Message queues
	 */
	public HashMap<String, Vector<TaggedMessage>> getMessageQueues() {
		return messageQueues;
	}

	/**
	 * Returns the thread channel handlers
	 * 
	 * @return
	 */
	public HashMap<String, Thread_ChannelHandler> getChannels() {
		return channels;
	}

	/**
	 * Returns the IRC controller
	 * 
	 * @return	The IRC controller
	 */
	public IRCController getIrcBot() {
		return ircBot;
	}
	
}
