package main;

import input.TaggedMessage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.PircBot;

import utilities.Utilities;

/**
 * IRC Controller used to receieve and send messages
 * 
 * @author Cadorn
 *
 */
public class IRCController extends PircBot {

	private BotController bot;
	private static final String hostname = "irc.twitch.tv";
	private static final int port = 6667;
	private String channelName;
	private String pass;

	public IRCController(BotController bot) {
		this.bot = bot;
		retrieveCredentials();
		this.setVerbose(false);
		this.setMessageDelay(0);

		try {

			this.connect(hostname, port, pass);
		} catch (IOException | IrcException e) {

			e.printStackTrace();
		}
	}

	/**
	 * Retrieves the credentials for the IRC client
	 */
	private void retrieveCredentials() {
		// TODO Find a better way of saving and loading credentials
		String credentials = null;
		
		credentials =  Utilities.readFile("./credentials");
		
		if (credentials == null) {
			// No credentials file, create one
			this.setName("");
			this.pass = "";
		} else {
			Scanner input = new Scanner(credentials);
			this.setName(input.nextLine());
			this.pass= input.nextLine();
		}
	}
	
	/**
	 * On receiving a message, create the tagged input and pass it along
	 * to be queued
	 */
	public void onMessage(String channel, String sender,
			String login, String hostname, String message) {
		ArrayList<String> tags = new ArrayList<String>();
		tags.add("CHAT");
		tags.add(sender);
		tags.add(channel);
		bot.queueMessage(new TaggedMessage(tags, message));
	}

	/**
	 * On someone joining a channel, create a join message and pass it along
	 */
	public void onJoin(String channel, String sender, String login, String hostname)  {
		ArrayList<String> tags = new ArrayList<String>();
		tags.add("JOIN");
		tags.add(sender);
		tags.add(channel);
		bot.queueMessage(new TaggedMessage(tags, sender));
	}
	
	/**
	 * Send a message to the current channel
	 * @param message Message to be sent
	 */
	public void sendMessage(String message) {
		this.sendMessage(channelName, message);
	}
	
	/**
	 * Gets the channel name
	 * 
	 * @return Name of the current channel
	 */
	public String getChannelName() {
		return this.channelName;
	}
}
