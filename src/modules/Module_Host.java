package modules;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import main.BotController;
import threads.Thread_ChannelHandler;
import threads.Thread_SQL;
import utilities.Utilities;
import input.TaggedMessage;

/**
 * Host module for Cadbot's channel
 * Handles the creation of other channel handlers
 * 
 * @author Cadorn
 *
 */
public class Module_Host extends Module {

	private Thread_ChannelHandler bot;
	private BotController controller;

	private static final String TOKEN_JOIN = "!join";
	private static final String TOKEN_LEAVE = "!leave";

	private ArrayList<String> activeChannels;

	public Module_Host(Thread_ChannelHandler bot, BotController controller) {
		this.bot = bot;
		this.controller = controller;

		this.activeChannels = new ArrayList<String>();
		initializeChannels();
	}

	/**
	 * Handles input
	 * Handles joining, leaving Cadbot
	 */
	public void handleInput(TaggedMessage input) {

		String message = input.getMessage();
		
		// Offer help
		if (checkHelp(input.getMessage())) {
			help();
			return;
		}

		// We want to create a new user thread
		if (message.equals(TOKEN_JOIN) && !activeChannels.contains(input.getTag(1))) {
			// Add the user channel thread
			controller.addUserChannel("#" + input.getTag(1));
			bot.sendMessage("Creating you a Cadbot, " + input.getTag(1) + "!");

			// Add it to the list and write it out
			addChannel(input.getTag(1));

			System.out.printf("Creating Cadbot for <%s>\n", "#" + input.getTag(1));
		} else if (message.equalsIgnoreCase(TOKEN_LEAVE) && activeChannels.contains(input.getTag(1))) {
			// Remove the user channel thread
			controller.removeUserChannel("#" + input.getTag(1));
			bot.sendMessage("Removing your Cadbot, " + input.getTag(1) + "!");

			// Remove it from the list and write it out
			removeChannel(input.getTag(1));

			System.out.printf("Removing Cadbot for <%s>\n", "#" + input.getTag(1));
		}

	}
	
	/**
	 * Creates a new channel handler and adds it to the bot controller
	 * 
	 * @param channel Name of the channel to add
	 */
	private void addChannel(String channel) {
		activeChannels.add(channel);
		String sql = "insert into Channels values(?)";
		
		ArrayList<String> args = new ArrayList<String>();
		args.add(channel);
		
		Thread_SQL thread = new Thread_SQL(sql, args, Thread_SQL.SQL_UPDATE);
		thread.start();
	}
	
	/**
	 * Removes a channel handler from the bot controller
	 * 
	 * @param channel Name of the channel to remove
	 */
	private void removeChannel(String channel) {
		activeChannels.remove(channel);
		String sql = "delete from Channels where channel = ?";
		ArrayList<String> args = new ArrayList<String>();
		args.add(channel);
		
		Thread_SQL thread = new Thread_SQL(sql, args, Thread_SQL.SQL_UPDATE);
		thread.start();
	}

	/**
	 * Initializes all of the channels that are stored
	 */
	private void initializeChannels() {
		readChannels();

		for (String s: activeChannels) {
			controller.addUserChannel("#" + s);
		}
	}

	/**
	 * Reads the channels that are subscribed to cadbot from the database
	 */
	private void readChannels() {
		String sql = "select channel from Channels";
		ArrayList<String> args = new ArrayList<String>();
		
		ResultSet results = Utilities.executeSQLQuery(sql, args);
		
		try {
			while(results.next()) {
				this.activeChannels.add(results.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Halts the module
	 * Doesn't have to do anything
	 */
	public void halt() {
		// No cleanup needs to be done
	}

	/**
	 * Prints help
	 */
	public void help() {
		bot.sendMessage("Type \"join\" to add Cadbot to your channel! Type \"!leave\" to remove him from your channel!");
	}

	/**
	 * Gets the name of the module
	 */
	public String getName() {
		return "";
	}
	
}
