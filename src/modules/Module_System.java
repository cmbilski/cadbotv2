package modules;

import java.util.Vector;

import threads.Thread_ChannelHandler;
import input.TaggedMessage;
import main.BotController;

/**
 * System module
 * Handles adding and removing from the whitelist/blacklist
 * 
 * @author Cadorn
 *
 */
public class Module_System extends Module {

	private Thread_ChannelHandler bot;
	
	private static final String TOKEN_WHITELIST_ADD = "!addwhitelist";
	private static final String TOKEN_BLACKLIST_ADD = "!addblacklist";
	private static final String TOKEN_BLACKLIST_REMOVE = "!removeblacklist";
	private static final String TOKEN_WHITELIST_REMOVE = "!removewhitelist";
	
	public Module_System(Thread_ChannelHandler bot) {
		this.bot = bot;
	}
	
	/**
	 * Handles input
	 * Watches for adding/removing from the whitelist/blacklist
	 */
	public void handleInput(TaggedMessage input) {

		String sender = input.getTag(1);
		
		// Only whitelisted members can use the system command
		if (!bot.checkWhitelist(sender)) {
			return;
		}
		
		if (checkHelp(input.getMessage())) {
			help();
			return;
		}
		
		if (input.getMessage().equalsIgnoreCase("!help")) {
			helpOverview();
			return;
		}
		
		String [] tokens = input.getMessage().split(" ");
		
		if (tokens.length == 2) {
			if (tokens[0].equals(TOKEN_WHITELIST_ADD)) {
				bot.addToWhitelist(tokens[1]);
				bot.sendMessage(tokens[1] + " has been added to the whitelist!");
				System.out.printf("SYSTEM: %s added to whitelist\n", tokens[1]);
			}
			if (tokens[0].equals(TOKEN_BLACKLIST_ADD)) {
				bot.addToBlacklist(tokens[1]);
				bot.sendMessage(tokens[1] + " has been added to the blacklist!");
				System.out.printf("SYSTEM: %s added to blacklist\n", tokens[1]);
			}
			if (tokens[0].equals(TOKEN_BLACKLIST_REMOVE)) {
				bot.removeFromBlackList(tokens[1]);
				bot.sendMessage(tokens[1] + " has been removed from the blacklist!");
				System.out.printf("SYSTEM: %s removed from blacklist\n", tokens[1]);
			}
			if (tokens[0].equals(TOKEN_WHITELIST_REMOVE)) {
				bot.removeFromWhiteList(tokens[1]);
				bot.sendMessage(tokens[1] + " has been removed from the whitelist!");
				System.out.printf("SYSTEM: %s removed from whitelist\n", tokens[1]);
			}
		}
		
		
	}
	
	/** 
	 * Halts the module
	 */
	public void halt() {
		// No cleanup needs to done
	}

	/**
	 * Prints out the currently installed modules
	 */
	public void helpOverview() {
		// We want to print out the currently loaded modules
		String moduleString = "";
		
		System.out.println("Help overview! Away!");
		
		Vector<Module> modules = bot.getModules();
		
		synchronized (modules) {
			for (int i = 0; i < modules.size() - 1; i++) {
				moduleString = moduleString + modules.get(i).getName() + ",";
			}
			moduleString = moduleString + modules.get(modules.size() - 1).getName();
		}
		
		bot.sendMessage("The following modules are currently installed:" + moduleString
				+ ". Type \"!help (module name)\" to receive help for a module.");
	}
	
	/**
	 * Prints help
	 */
	public void help() {
		// Currently, I don't want to give any information about the whitelist commands
		helpOverview();
	}

	/**
	 * Gets the name of the module
	 */
	public String getName() {
		return " system";
	}
	
}
