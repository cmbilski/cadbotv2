package modules;

import input.TaggedMessage;

/**
 * Abstract class which all modules are based off of
 * 
 * @author Cadorn
 *
 */
public abstract class Module {
	
	protected boolean initialized;

	/**
	 * Handles input
	 * 
	 * @param input
	 */
	public abstract void handleInput(TaggedMessage input);
	
	/**
	 * Halts the module
	 * Performs any cleanup that is necessary
	 */
	public abstract void halt();
	
	/**
	 * Prints module-specific help
	 */
	public abstract void help();
	
	/**
	 * Gets the name of the module
	 * 
	 * @return
	 */
	public abstract String getName();
	
	/**
	 * Checks if the input is a help command for this module
	 * 
	 * @param message
	 * @return
	 */
	protected boolean checkHelp(String message) {
		return message.equalsIgnoreCase("!help" + getName());
	}
	
	/**
	 * Returns if the module is initialized or not
	 * 
	 * @return
	 */
	public boolean isInitiailized() {
		return initialized;
	}
	
}
