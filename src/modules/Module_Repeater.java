package modules;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

import threads.Thread_ChannelHandler;
import threads.Thread_Repeater;
import threads.Thread_SQL;
import utilities.Utilities;
import input.TaggedMessage;

/**
 * Repeats a series of phrases once every interval of time
 * 
 * @author Cadorn
 *
 */
public class Module_Repeater extends Module {

	private Thread_ChannelHandler bot;
	private Thread_Repeater repeater;

	private static final String TOKEN_ADD = "!addphrase";
	private static final String TOKEN_REMOVE = "!removephrase";
	private static final String TOKEN_CHANGE = "!changephraseinterval";
	private static final String TOKEN_TEST = "!testphrase";
	private static final String TOKEN_CLEAR = "!clearphrases";
	
	private int currentPhrase;
	
	private Vector<String> phrases;

	public Module_Repeater(Thread_ChannelHandler bot) {
		this.bot = bot;
		
		currentPhrase = 0;

		// Try and find any initial phrases
		initializePhrases();
		
		// Make the repeater thread
		repeater = new Thread_Repeater(this);
		repeater.start();
	}

	
	/**
	 * Gets the phrases from the database and loads them into memory
	 */
	private void initializePhrases() {
		// Create the phrase structure
		phrases = new Vector<String>();
		
		
		// Pull down from the database
		String sql = "select phrase from Repeater where host = ?";
		ArrayList<String> args = new ArrayList<String>();
		args.add(bot.getChannelName());
		
		ResultSet results = Utilities.executeSQLQuery(sql, args);
		
		try {
			while(results.next()) {
				this.phrases.add(results.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Handles input
	 * Watches for add/remove commands
	 */
	public void handleInput(TaggedMessage input) {

		// We only want to take commands from whitelisted users
		if (!bot.checkWhitelist(input.getTag(1))) {
			return;
		}

		if (input.getMessage().equalsIgnoreCase("!help" + this.getName())) {
			help();
			return;
		}
		
		String [] tokens = input.getMessage().split(" ");

		if (tokens[0].equalsIgnoreCase(TOKEN_ADD)) {
			addPhrase(input.getMessage());
		} else if (tokens[0].equalsIgnoreCase(TOKEN_REMOVE)) {
			removePhrase(input.getMessage());
		} else if (tokens[0].equalsIgnoreCase(TOKEN_CHANGE)) {
			changeInterval(input.getMessage());
		} else if(tokens[0].equalsIgnoreCase(TOKEN_TEST)) {
			sayPhrase();
		} else if (tokens[0].equalsIgnoreCase(TOKEN_CLEAR)) {
			clearPhrases();
		}

	}

	/**
	 * Adds a phrase to repeat
	 * 
	 * @param message	The phrase to add
	 */
	public void addPhrase(String message) {
		String newMessage = message.substring(message.indexOf(" ") + 1);

		// We don't want to add empty messages
		if (newMessage.isEmpty()) {
			return;
		}

		if (phrases.contains(newMessage)) {
			// We don't want duplicates
			// Return an error
			bot.sendMessage("I already know that phrase!");
		} else {
			// Add the message
			this.phrases.addElement(newMessage);
			bot.sendMessage("Phrase added!");
			
			// Construct the sql
			String sql = "insert into Repeater values(?, ?)";
			ArrayList<String> args = new ArrayList<String>();
			args.add(bot.getChannelName());
			args.add(newMessage);
			
			// Fire off the sql thread
			Thread_SQL sqlThread = new Thread_SQL(sql, args, Thread_SQL.SQL_UPDATE);
			sqlThread.start();
		}
	}

	/**
	 * Removes a phrase from the series
	 * 
	 * @param message	The phrase to remove
	 */
	public void removePhrase(String message) {
		String newMessage = message.substring(message.indexOf(" ") + 1);

		// We don't want to add empty messages
		if (newMessage.isEmpty()) {
			return;
		}

		if (phrases.remove(newMessage)) {
			// Display a success message to chat
			bot.sendMessage("Phrase removed!");

			// Construct the sql
			String sql = "delete from Repeater where host = ? and phrase = ?";
			ArrayList<String> args = new ArrayList<String>();
			args.add(bot.getChannelName());
			args.add(newMessage);
			
			// Fire off the sql thread
			Thread_SQL sqlThread = new Thread_SQL(sql, args, Thread_SQL.SQL_UPDATE);
			sqlThread.start();
			
		} else {
			// Otherwise, an error
			bot.sendMessage("I don't know that phrase!");
		}
	}

	/**
	 * Changes the interval that the message will be repeated at
	 * 
	 * @param message
	 */
	public void changeInterval(String message) {
		// Hold on, we don't want to do this yet
		//TODO
		
	}

	/**
	 * Clears out all phrases from the series
	 * 
	 */
	private void clearPhrases() {
		// Clear it out and send acknowledgement
		phrases.clear();
		bot.sendMessage("Phrases cleared!");

		// Construct the sql
		String sql = "delete from Repeater where host = ?";
		ArrayList<String> args = new ArrayList<String>();
		args.add(bot.getChannelName());
		
		// Fire off the sql thread
		Thread_SQL sqlThread = new Thread_SQL(sql, args, Thread_SQL.SQL_UPDATE);
		sqlThread.start();
	}
	
	/**
	 * Halts the module
	 * Stops the thread which controls repeating
	 */
	public void halt() {
		repeater.halt();
	}

	/**
	 * Prints help
	 */
	public void help() {
		// We aren't going to tell them about test phrase yet
		bot.sendMessage("Repeater Commands: \"!addphrase (phrase)\", \"!removephrase (phrase)\", \"!clearphrases\"");

	}

	/**
	 * Grabs the next phrase from the series and prints it
	 */
	public void sayPhrase() {
		// Say some phrase from our list
		// How we choose can depend on a chosen schema
		
		// First thing's first, if the stream is offline, don't bother
		if (!Utilities.isStreamOnline(bot.getChannelName().substring(1))) {
			return;
		}
		
		// For now, we will use a circular rotation
		if (phrases.size() > 0) {
			currentPhrase %= phrases.size();
			bot.sendMessage(phrases.get(currentPhrase));
			currentPhrase = (currentPhrase + 1) % phrases.size();
		}
	}

	/**
	 * Gets the name of the module
	 */
	public String getName() {
		return " repeater";
	}

}
