package modules;

import input.TaggedMessage;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import threads.Thread_ChannelHandler;
import threads.Thread_FollowerManager;
import threads.Thread_SQL;
import utilities.Utilities;

/**
 * Notifies the streamer when a user followers
 * 
 * @author Cadorn
 *
 */
public class Module_FollowerNotification extends Module {

	private Thread_ChannelHandler bot;
	private HashMap<String, String> followers;
	private Thread_FollowerManager followerManager;

	public Module_FollowerNotification(Thread_ChannelHandler bot) {
		this.bot = bot;

		this.followers = new HashMap<String, String>();

		initializeFollowers();

		followerManager = new Thread_FollowerManager(this);
		followerManager.start();
	}

	/**
	 * Initializes the follower list
	 * Reads from the SQL and then pulls from the API
	 * 
	 */
	private void initializeFollowers() {
		// We want to pull down our followers from the table
		String sql = "select follower from Followers where host = ?";
		ArrayList<String> args = new ArrayList<String>();
		args.add(bot.getChannelName());

		ResultSet followers = Utilities.executeSQLQuery(sql, args);
		int count = 0;

		// Add all of the followers we know about
		try {
			while (followers.next()) {
				count++;
				String follower = followers.getString(1);
				this.followers.put(follower, follower);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// Now we will grab the list from the api and add those
		updateFollowers(0);

	}

	/**
	 * Makes a twitch api call to get the followers of the streamer
	 * Adds them to the list and sends a message to any new ones
	 * 
	 * @param verbose
	 */
	public void updateFollowers(int verbose) {
		// Identical to the initialization except we will now send a chat message along
		// If there is any follower that we don't have logged, we will send a message
		JSONArray followerArray = getFollowers();
		if (followerArray == null) {
			return;
		}

		ArrayList<ArrayList<String>> batchArgs = new ArrayList<ArrayList<String>>();
		
		// Iterate through the returned followers
		for (int i = 0; i < followerArray.length(); i++) {
			String username = followerArray.getJSONObject(i).getJSONObject("user").getString("name");

			// Check against our records
			String log = followers.get(username);
			if (log == null) {
				// If we don't know about them yet, add them
				followers.put(username, username);
				
				// Some messaging if we want to
				if (verbose == 1) {
					System.out.printf("FOLLOWER: New follower %s\n", username);
					bot.sendMessage(String.format("Thanks for following, %s! <3", username));
				}
				
				// Add this to the list of new followers
				ArrayList<String> args = new ArrayList<String>();
				args.add(bot.getChannelName());
				args.add(username);
				
				// Add it to the batch args
				batchArgs.add(args);
				
			}
		}
		
		// We don't want to send empty updates
		if (batchArgs.isEmpty()) {
			return;
		}
		
		String sql = "insert into Followers values (?, ?)";
		
		// Execute a batch insert
		Thread_SQL thread = new Thread_SQL(sql, batchArgs, 2);
		thread.start();
		
	}

	/**
	 * Makes a call to the Twitch API to get the followers
	 * 
	 * @return	a JSON array of followers
	 */
	private JSONArray getFollowers() {
		String channelName = bot.getChannelName().substring(1);

		String request = String.format("https://api.twitch.tv/kraken/channels/%s/follows", 
				channelName);

		URL url;
		HttpURLConnection connection = null;

		try {
			url = new URL(request);
			connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/vnd.twitchtv.v2+json");

			connection.setUseCaches(false);
			connection.setDoOutput(true);
			connection.setDoInput(true);

			int responseCode = connection.getResponseCode();
			//System.out.println("FOLLOWER: Received code " + responseCode);
			if (responseCode != 200) {
				return null;
			}

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line ;
			StringBuffer response = new StringBuffer();

			while ((line = in.readLine()) != null) {
				response.append(line);
			}

			connection.disconnect();
			in.close();

			JSONObject jsonResponse = new JSONObject(response.toString());
			return jsonResponse.getJSONArray("follows");

		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Handles input
	 * Prints help
	 */
	public void handleInput(TaggedMessage input) {
		if (checkHelp(input.getMessage())) {
			help();
			return;
		}
	}
	
	/**
	 * Halts the module
	 * Stops the follower manager thread
	 */
	public void halt() {
		// Shut down the follower thread
		this.followerManager.halt();
	}

	/**
	 * Prints help
	 */
	public void help() {
		// I'm really not sure what help needs to be displayed for this module...
		bot.sendMessage("Follower Commands: The Follower Module has no commands");
	}

	/**
	 * Returns the name of the module
	 */
	public String getName() {
		return " followers";
	}

}
