package modules;

import input.TaggedMessage;

import java.util.ArrayList;

import threads.Thread_ChannelHandler;
import main.BotController;

/**
 * Greets people who join the channel
 * 
 * @author Cadorn
 *
 */
public class Module_Greeter extends Module {

	private static final String TOKEN = "JOIN";
	private static final String TOKEN_CHANGE_GREETING = "!changegreeting";
	private static final String TOKEN_DISABLE_GREETING = "!disablegreeting";
	private static final String TOKEN_ENABLE_GREETING = "!enablegreeting";
	private String greeting;

	// Greetings start of disabled
	private boolean enableGreeting;

	private Thread_ChannelHandler bot;

	public Module_Greeter(Thread_ChannelHandler bot) {
		this.bot = bot;
		this.greeting = "Hello %s! <3";
		this.enableGreeting = false;
	}

	/**
	 * Checks if the message is a join message
	 * 
	 * @param tags
	 * @return If the message is a join message
	 */
	private boolean checkTag(ArrayList<String> tags) {
		return (tags.get(0).compareTo(TOKEN) == 0);
	}

	/**
	 * Handles input
	 * If the message is a join message, print out a greeting
	 * 
	 */
	public void handleInput(TaggedMessage input) {
		
		if (checkTag(input.getTag())) {

			System.out.println("GREETER: " + input.getMessage() + " joined the channel");

			if (enableGreeting) {
				
				String message = String.format(greeting, input.getMessage());
				bot.sendMessage(message);

				return;
			}

		}

		// We only want whitelisted people to be able to change greetings settings
		if (!bot.checkWhitelist(input.getTag(1))) {
			return;
		}
		
		// Display help
		if (checkHelp(input.getMessage())) {
			help();
			return;
		}
		
		String [] tokens = input.getMessage().split(" ");

		if (bot.checkWhitelist(input.getTag(1)) && tokens.length >= 2) {

			if (tokens[0].equals(TOKEN_CHANGE_GREETING)) {
				greeting = input.getMessage().substring(input.getMessage().indexOf(" "));

				bot.sendMessage("Your greeting has been changed!");
				System.out.println("GREETER: Greeting changed to: " + greeting);
			}

		} else {
			if (tokens[0].equals(TOKEN_DISABLE_GREETING)) {
				enableGreeting = false;
				bot.sendMessage("Greetings disabled!");
				System.out.println("GREETER: Greeting disabled");
			}

			if (tokens[0].equals(TOKEN_ENABLE_GREETING)) {
				enableGreeting = true;
				bot.sendMessage("Greetings enabled!");
				System.out.println("GREETER: Greeting enabled");
			}
		}
		
	}

	/**
	 * Halt the module
	 * Nothing has to be done here
	 */
	public void halt() {
		// No cleanup needs to be done
	}

	/**
	 * Prints help
	 */
	public void help() {
		bot.sendMessage("Greeter Commands: !enablegreeting, !disablegreeting, !changegreeting (new greeting)");
	}
	
	/**
	 * Gets the name of the model
	 */
	public String getName() {
		return " greeter";
	}
	
}
