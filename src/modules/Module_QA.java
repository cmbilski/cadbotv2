package modules;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import threads.Thread_ChannelHandler;
import threads.Thread_SQL;
import utilities.Utilities;
import input.TaggedMessage;

/**
 * Allows the streamer to define command-response pairs for cadbot to respond to in chat
 * 
 * @author Cadorn
 *
 */
public class Module_QA extends Module {

	private HashMap<String, String> qa;
	private Thread_ChannelHandler bot;
	
	private static final String TOKEN_ADD_COMMAND = "!addcommand";
	private static final String TOKEN_REMOVE_COMMAND = "!removecommand";
	
	private String qaName;
	
	public Module_QA(Thread_ChannelHandler bot) {
		this.bot = bot;
		
		this.qaName = String.format("./%s/modules/qa", bot.getChannelName());
		
		initializeQA();
	}
	
	/**
	 * Pulls all of the pairs from the database and loads them into memory
	 * 
	 */
	private void initializeQA() {
		qa = new HashMap<String, String>();
		
		ArrayList<String> args = new ArrayList<String>();
		args.add(bot.getChannelName());
		
		// Select all of the commands and their responses for this host
		String qaSelect = "select command, response from Commands where host = ?";
		ResultSet results = Utilities.executeSQLQuery(qaSelect, args);
		
		// For all of the results, add them to the list
		try {
			while (results.next()) {
				qa.put(results.getString(1), results.getString(2));
			}
		} catch (SQLException e) {
		}

		
	}
	
	/**
	 * Handles input
	 * Watches for a command it can respond to or watches for an add/remove command
	 */
	public void handleInput(TaggedMessage input) {
		String answer = qa.get(input.getMessage());
		if (answer != null) {
			bot.sendMessage(answer);
			System.out.printf("QA: Responding to <%s> with <%s>\n", input.getMessage(), answer);
			return;
		}
		
		// Only whitelisted members can change the commands
		if (!bot.checkWhitelist(input.getTag(1))) {
			return;
		}
		
		if (checkHelp(input.getMessage())) {
			help();
			return;
		}
		
		String [] tokens = input.getMessage().split(" ");
		
		// There needs to be at least three tokens
		// Command, new command, response
		if (tokens.length >= 3) {
			
			// Add a new command
			if (tokens[0].equals(TOKEN_ADD_COMMAND)) {
				// The first letter of the second token needs to be "!" to indicate a new command
				if (tokens[1].charAt(0) != '!') {
					return;
				}
				
				// We need to get the response parsed out
				// This should be the substring starting wtih the second "!"
				String substring = input.getMessage().substring(input.getMessage().indexOf(" ") + 1);
				
				// This will be the new response, starting immediately after the second space
				String newResponse = substring.substring(substring.indexOf(" ") + 1);
				writeCommand(tokens[1], newResponse);
				this.qa.put(tokens[1], newResponse);
				bot.sendMessage("Command \"" + tokens[1] + "\" has been added!");
			}
		} else if (tokens[0].equals(TOKEN_REMOVE_COMMAND) && tokens.length == 2) {
			// We want to remove tokens[1] now
			if (this.qa.remove(tokens[1]) != null) {
				removeCommand(tokens[1]);
				bot.sendMessage("Command \"" + tokens[1] + "\" has been removed!");
			}
			
		}
		
	}
	
	/**
	 * Remove a command response pair from memory
	 * 
	 * @param command The command to remove
	 */
	private void removeCommand(String command) {
		// Form the delete sql
		String sql = "delete from Commands where host = ? and command = ?";
		ArrayList<String> args = new ArrayList<String>();
		
		// Add the channel name and command
		args.add(bot.getChannelName());
		args.add(command);
		
		// Spawn the thread and start it
		Thread_SQL thread = new Thread_SQL(sql, args, 1);
		thread.start();
	}
	
	/**
	 * Adds a command response pair to memory
	 * 
	 * @param command	Command to add
	 * @param response	Response to add
	 */
	private void writeCommand(String command, String response) {
		// If the command currently exists, we will issue an update command
		String sql;
		ArrayList<String> args = new ArrayList<String>();
		if (qa.get(command) != null) {
			// Command already exists
			// Update
			sql = "update Commands set response = ? where command = ? and host = ?";
			args.add(response);
			args.add(command);
			args.add(bot.getChannelName());
		} else {
			// Command doesn't exist yet
			// Insert
			
			sql = "insert into Commands values(?, ?, ?)";
			args.add(bot.getChannelName());
			args.add(command);
			args.add(response);
		}
		
		// Spawn the thread and start it
		Thread_SQL thread = new Thread_SQL(sql, args, 1);
		thread.start();
		
	}
	
	/**
	 * Halt the module
	 * Nothing to do here
	 */
	public void halt() {
		// No cleanup needs to be done
	}

	/**
	 * Prints help
	 */
	public void help() {
		bot.sendMessage("Command Module Commands: \"!addcommand !(new command) (response)\" to create a new command and response. " +
				"\"!removecommand !(command)\" to remove that command.");
	}
	
	/** 
	 * Gets the name of the module
	 */
	public String getName() {
		return " commands";
	}

}
