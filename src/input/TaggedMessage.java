package input;

import java.util.ArrayList;

// Message type tag 1
// Message sender tag 2
// Message channel tag 3
public class TaggedMessage {

	private String message;
	private ArrayList<String> tags;
	
	public  TaggedMessage(ArrayList<String> tags, String message) {
		this.message = message;
		this.tags = tags;
	}
	
	/**
	 * Get all the tags
	 * 
	 * @return
	 */
	public ArrayList<String> getTag() {
		return this.tags;
	}
	
	/**
	 * Get the tag at a specific index
	 * Message type tag 1
	 * Message sender tag 2
	 * Message channel tag 3
	 * 
	 * @param index The index of the tag
	 * @return Tag at that index
	 */
	public String getTag(int index) {
		return this.tags.get(index % this.tags.size());
	}
	
	/**
	 * Get the message
	 * 
	 * @return
	 */
	public String getMessage() {
		return this.message;
	}
}
