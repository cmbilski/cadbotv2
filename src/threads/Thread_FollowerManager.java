package threads;

import modules.Module_FollowerNotification;

/**
 * Signals the follower notification module to check followers
 * 
 * @author Cadorn
 *
 */
public class Thread_FollowerManager extends Thread {

	private static final int SECONDS_TO_SLEEP = 15;
	
	private Module_FollowerNotification module;
	
	private boolean shouldExecute;
	
	public Thread_FollowerManager(Module_FollowerNotification module) {
		this.module = module;
		this.shouldExecute = true;
	}
	
	/**
	 * Signals the follower module to check followers and then sleeps some interval
	 * Exits when the follower module closes
	 */
	public void run() {
		while (shouldExecute) {
			// Run until we close
			
			// We will check followers every 15 seconds
			try {
				Thread.sleep(SECONDS_TO_SLEEP * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			module.updateFollowers(1);
		}
	}
	
	/**
	 * Halts the thread
	 */
	public void halt() {
		this.shouldExecute = false;
	}
	
}
