package threads;

import input.TaggedMessage;

import java.util.HashMap;
import java.util.Vector;

import main.BotController;
import main.IRCController;
import modules.Module_FollowerNotification;
import modules.Module_Greeter;
import modules.Module_QA;
import modules.Module_Repeater;
import modules.Module_System;

/**
 * Creates a channel handler thread for the given channel
 * 
 * @author Cadorn
 *
 */
public class Thread_ChannelCreator extends Thread {

	private BotController bot;
	private String name;
	
	public Thread_ChannelCreator(BotController bot, String name) {
		this.bot = bot;
		this.name = name;
	}

	/**
	 * Creates a channel handler thread for the given thread
	 * Exits when creation is finished
	 */
	public void run() {
		// We don't want duplicate channels
		if (bot.getChannels().get(name) != null) {
			return;
		}

		// Create the message queue and channel
		Vector<TaggedMessage> newQueue = new Vector<TaggedMessage>();
		Thread_ChannelHandler newChannel = new Thread_ChannelHandler(bot, newQueue, name);

		// Load the relevant modules into thread
		// System is always going to be the first module
		// So has it been written, so shall it be done
		newChannel.addModule(new Module_System(newChannel));
		newChannel.addModule(new Module_Greeter(newChannel));
		newChannel.addModule(new Module_FollowerNotification(newChannel));
		newChannel.addModule(new Module_QA(newChannel));
		newChannel.addModule(new Module_Repeater(newChannel));

		// Store the thread and queue into maps
		bot.getMessageQueues().put(name, newQueue);
		bot.getChannels().put(name, newChannel);

		// Start the thread
		newChannel.start();

		bot.getIrcBot().joinChannel(name);
	}

}
