package threads;

import java.util.ArrayList;

import utilities.Utilities;

/**
 * Executes a SQL statement which we don't need a return value from
 * Prevents the main threads from blocking until the sql finishes
 * 
 * @author Cadorn
 *
 */
public class Thread_SQL extends Thread {

	public static final int SQL_QUERY = 0;
	public static final int SQL_UPDATE = 1;
	public static final int SQL_BATCH_UPDATE = 2;
	
	private String command;
	private Object args;
	private int type;

	public Thread_SQL(String command, Object args, int type) {
		this.command = command;
		this.args = args;
		this.type = type;
	}

	/**
	 * Executes the given SQL statement
	 */
	@SuppressWarnings("unchecked")
	public void run() {
		// So, for now, type 0 is a Query thread
		// All other types are update threads
		// We will find a better way to handle this eventually
		if (type == SQL_QUERY) {
			Utilities.executeSQLQuery(command, (ArrayList<String>) args);
		} else if (type == SQL_UPDATE) {
			Utilities.executeSQLUpdate(command, (ArrayList<String>) args);
		} else if (type == SQL_BATCH_UPDATE) {
			Utilities.executeSQLBatchUpdate(command, (ArrayList<ArrayList<String>>) args);
		}

	}

}
