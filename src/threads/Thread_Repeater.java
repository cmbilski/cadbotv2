package threads;

import modules.Module_Repeater;

/**
 * Signals to the repeater module to say a phrase
 * 
 * @author Cadorn
 *
 */
public class Thread_Repeater extends Thread {

	private boolean shouldRun;
	private Module_Repeater module;
	
	// Default wait is 10 minutes
	private static final long WAIT_INTERVAL_DEFAULT = 10;
	private long waitInterval = WAIT_INTERVAL_DEFAULT * 60 * 1000;
	
	public Thread_Repeater(Module_Repeater module) {
		this.module = module;
		this.shouldRun = true;
	}
	
	/**
	 * Signals to the repeater module to say a phrase
	 * Sleeps an interval afterwards
	 */
	public void run() {
		while (shouldRun) {
			// Wait an interval
			try {
				Thread.sleep(waitInterval);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			// Make the module repeat a phrase
			module.sayPhrase();
		}
	}
	
	/** 
	 * Halts the thread
	 */
	public void halt() {
		this.shouldRun = false;
	}
}
