package threads;

import input.TaggedMessage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

import utilities.Utilities;
import main.BotController;
import main.MessageController;
import modules.Module;

/**
 * Controls all the modules and other information for a specific channel
 * 
 * @author Cadorn
 *
 */
public class Thread_ChannelHandler extends Thread {

	private BotController bot;
	
	private volatile Vector<TaggedMessage> messageQueue;
	private Vector<Module> modules;
	private String channelName;
	
	private ArrayList<String> whitelist;
	private ArrayList<String> blacklist;
	
	private volatile boolean shouldRun;
	
	private MessageController messageController;
	
	public Thread_ChannelHandler(BotController bot, Vector<TaggedMessage> mQ, String channelName) {
		this.bot = bot;
		this.messageQueue = mQ;
		this.modules = new Vector<Module>();
		this.channelName = channelName;
		this.shouldRun = true;
		
		this.messageController = new MessageController(bot, this.channelName);
		prepareLists();
				
		System.out.printf("%s is online!\n", this.channelName);
	}

	/**
	 * Reads from the message queue indefinitely
	 * Exits when the botcontroller closes
	 */
	public void run() {
		while (shouldRun) {		
			// We want to keep running
			while (!messageQueue.isEmpty()) {
				// We will handle all messages in the queue before sleeping

				// Remove the first message
				TaggedMessage message = messageQueue.remove(0);
				
				// If the message is from a blacklisted source, ignore it
				if (this.checkBlacklist(message.getTag(1))) {
					continue;
				}

				// Pass it to all of the modules
				for (Module m: modules) {
					m.handleInput(message);
				}
			} // End of message loop

			// Wait until we are woken up again
			this.lock();

		} // End of running loop

	}
	
	public void help() {
		modules.get(0).help();
	}
	
	public void halt() {
		// Stop this thread
		this.shouldRun = false;
		
		// Stop all of the modules
		for (Module m: modules) {
			m.halt();
		}
	}

	public void unlock() {
		synchronized(messageQueue) {
			this.messageQueue.notify();
		}
	}

	private void lock() {
		synchronized(messageQueue) {
			try {
				this.messageQueue.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void sendMessage(String message) {
		messageController.addMessage(message);
	}

	private void handleWhitelist() {
		// Grab the blacklist from the database
		String sql = "select user from Whitelist where host = ?";
		ArrayList<String> args = new ArrayList<String>();
		args.add(this.getChannelName());
				
		ResultSet results = Utilities.executeSQLQuery(sql, args);
		
		try {
			while(results.next()) {
				whitelist.add(results.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		// If we don't have anything, whitelist a few automatically
		if (whitelist.isEmpty()) {
			// Whitelist the streamer
			this.addToWhitelist(this.getChannelName().substring(1));
			// Whitelist Cadorn
			this.addToWhitelist("cadorn");
		}
	}
	
	private void handleBlacklist() {
		// Grab the blacklist from the database
		String sql = "select user from Blacklist where host = ?";
		ArrayList<String> args = new ArrayList<String>();
		args.add(this.getChannelName());
		
		ResultSet results = Utilities.executeSQLQuery(sql, args);
		
		try {
			while(results.next()) {
				blacklist.add(results.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		// If we don't have anything, blacklist a few automatically
		if (blacklist.isEmpty()) {
			// Ignore myself
			this.addToBlacklist("cadbot");
			// Ignore twitchranks
			this.addToBlacklist("twitchranks");
		}
	}
	
	private void prepareLists() {
		// Read in the whitelist
		String whiteList = null;
	
		this.whitelist = new ArrayList<String>();
		this.blacklist = new ArrayList<String>();

		// Get the whitelist
		handleWhitelist();
		
		// Get the blacklist
		handleBlacklist();
	}

	public boolean checkWhitelist(String name) {
		return whitelist.contains(name);
	}

	public void addToWhitelist(String name) {
		if (whitelist.contains(name)) {
			return;
		}
		whitelist.add(name);
		
		String sql = "insert into Whitelist values(?, ?)";
		ArrayList<String> args = new ArrayList<String>();
		args.add(this.getChannelName());
		args.add(name);
		
		Thread_SQL thread = new Thread_SQL(sql, args, Thread_SQL.SQL_UPDATE);
		thread.start();
		
	}

	public boolean checkBlacklist(String name) {
		return blacklist.contains(name);
	}

	public void addToBlacklist(String name) {
		if (blacklist.contains(name)) {
			return;
		}
		blacklist.add(name);
		
		String sql = "insert into Blacklist values(?, ?)";
		ArrayList<String> args = new ArrayList<String>();
		args.add(this.getChannelName());
		args.add(name);
		
		Thread_SQL thread = new Thread_SQL(sql, args, Thread_SQL.SQL_UPDATE);
		thread.start();
	}
	
	public void removeFromBlackList(String name) {
		if (blacklist.remove(name) == true) {
			String sql = "delete from Blacklist where host = ? and user = ?";
			ArrayList<String> args = new ArrayList<String>();
			args.add(this.getChannelName());
			args.add(name);
			
			Thread_SQL thread = new Thread_SQL(sql, args, Thread_SQL.SQL_UPDATE);
			thread.start();
		}
	}
	
	public void removeFromWhiteList(String name) {
		if (whitelist.remove(name) == true) {
			String sql = "delete from Whitelist where host = ? and user = ?";
			ArrayList<String> args = new ArrayList<String>();
			args.add(this.getChannelName());
			args.add(name);
			
			Thread_SQL thread = new Thread_SQL(sql, args, Thread_SQL.SQL_UPDATE);
			thread.start();
		}
	}
	
	public void addModule(Module module) {
		this.modules.add(module);
	}
	
	public String getChannelName() {
		return this.channelName;
	}

	public Vector<Module> getModules() {
		return this.modules;
	}
	
}
