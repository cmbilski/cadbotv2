package threads;

import main.MessageController;

/**
 * Sends messages to the messanger
 * Only sends a limited number of messages per interval
 * 
 * @author Cadorn
 *
 */
public class Thread_MessageSender extends Thread {

	private MessageController messanger;

	public Thread_MessageSender(MessageController messanger) {
		this.messanger = messanger;
	}

	/**
	 * Sends messages to the messanger
	 * Only sends a limited number per interval
	 */
	public void run() {
		while (true) {
			// Check if we have messages, if not, wait until we do
			if (messanger.messages.size() == 0) {
				try {
					//System.out.println("No messages, waiting");
					synchronized (messanger.noMessageLock) {
						messanger.noMessageLock.wait();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				//System.out.println("Done waiting");

			}

			// See if we can send a message
			if (messanger.canSendMessage()) {
				messanger.sendMessage();
			} else {
				// Can't send a message, wait until we can
				try {
					synchronized (messanger.tooManyMessagesLock) {
						messanger.tooManyMessagesLock.wait();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		}
	}
}
