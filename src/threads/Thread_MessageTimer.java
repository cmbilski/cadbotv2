package threads;

import main.MessageController;

/**
 * Controls the intervals for which messages are sent
 * Resets the number of messages sent when an interval finishes
 * 
 * @author Cadorn
 *
 */
public class Thread_MessageTimer extends Thread {

	private MessageController messanger;
	
	public Thread_MessageTimer(MessageController messanger) {
		this.messanger = messanger;
	}
	
	/**
	 * Sleep for an interval
	 * When finished sleeping, reset the messages sent, a new interval has started
	 */
	public void run() {
		try {
			Thread.sleep(30 * 1000);
		} catch (InterruptedException e) {
		} finally {
			System.out.println("Resetting messages");
			messanger.resetMessages();
		}
	}
	
}
 